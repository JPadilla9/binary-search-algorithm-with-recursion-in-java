import java.util.*;

//Question 16 binary search of an array to locate the value aKey

public class binarySearch{

public static void main(String[] args) {
	
  int sorted[] = {36, 49, 39, 621, 1, -57, -30, 241, 987};
	Arrays.sort(sorted);
	System.out.println(Arrays.toString(sorted));
	int aKey = 987;
  int high = sorted.length - 1;
  int low = 0;
  int i = (high + low)/2;
  int count = 0;
  //everything above is the base case
  //while loop is the reduced problem or recursive part
  while(aKey != sorted[i] && high!=low){
  count++;
  if(sorted[i]>aKey) {
    high = i - 1;
    i = (high + low)/2;
    System.out.println("search value not found in upper half of array on the " +count+ " time, so high is now i - 1");
  }
  if(sorted[i]<aKey){
    low = i+1;
    i = (high + low)/2;
    System.out.println("search value not found in lower half of array on the " + count + " time so low is now i + 1");
  }}
  //General solution is this last if statement
   if(sorted[i] == aKey) {
     count++;
    System.out.println(aKey + " found at index " 
    +Arrays.binarySearch(sorted, aKey)+ " on the " +count+ " time");
      }
    }
}